console.log("Hello World");

/*
	1. Create variables to store to the following user details:

	-first name - String
	-last name - String
	-age - Number
	-hobbies - Array
	-work address - Object

		-The hobbies array should contain at least 3 hobbies as Strings.
		-The work address object should contain the following key-value pairs:

			houseNumber: <value>
			street: <value>
			city: <value>
			state: <value>

	Log the values of each variable to follow/mimic the output.

	Note:
		-Name your own variables but follow the conventions and best practice in naming variables.
		-You may add your own values but keep the variable names and values Safe For Work.
*/

	//Add your variables and console log for objective 1 here:




/*			
	2. Debugging Practice - Identify and implement the best practices of creating and using variables 
	   by avoiding errors and debugging the following codes:

			-Log the values of each variable to follow/mimic the output.
*/	

	let myFirstName = "Angelique";
	console.log("First Name:  " + myFirstName);
	
	let myLastName = "Cabacis";
	console.log("Last Name:  " + myLastName);


	let age = 32;
	console.log("Age: " + age);

	let myHobbiesAre = "My Hobbies are";
 	 console.log(myHobbiesAre);
	
	let hobbies = ["Cooking", "Designing", "Travelling"];
	console.log(hobbies);

	let myWorkAddress = "My work address";
 	 console.log(myWorkAddress);

	let workAddress = {
 	 		streetNumber: 102,
 	 		buildingName: 'Greenrich Mansion',
 	 		streetName: 'Pearl drive',
 	 		barangayName: 'Ortigas Center',
 	 		cityname: 'Pasig City'
 	 	}
 	 console.log(workAddress);

 	 let fullName = "Angelique Cabacis";
 	 console.log("My full Name is: " + fullName);

 	 let myCurrentAge = 32;
 	 console.log("My current Age is: " + myCurrentAge);

 	 let myFriendsAre = "My Friends are";
 	 console.log(myFriendsAre);

		let myFriends= ["Reine", "Che", "Seph", "Ian" ]
 	 			 	
 	 	
 	 console.log(myFriends);

		
		let myFullProfile = "My Full Profile";
		console.log("My Full Profile: ");

		let profile = {

		username: "gelcabs",
		name: "Angelique Cabacis",
		age: 32,
		isActive: false,
	}
	console.log(profile);


	

	let myBestFriend= "Reine";
	console.log("My bestfriend is: " + myBestFriend);

	let lastLocation = "Arctic Ocean";
	lastLocation = "Hawaii";
	console.log("I was found in: " + lastLocation);
